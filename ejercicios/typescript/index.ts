//BOOLEAN
let muted: boolean = true;
muted = false;

//NUMBER
let age = 6;
let numerador = 42;
let denominador: number = age;
let resultado = numerador / denominador;

//STRING
let nombre: string = "Richard";
let saludo = `Me llamo ${nombre}`;

//ARREGLOS

//Un solo tipo
let people: string[] = [];
people = ["Isabel", "Nicole", "Raul"];
people.push("9000");

//Varios tipos
let peopleAndNumbers: Array<string | number> = [];
peopleAndNumbers.push("Ricardo");
peopleAndNumbers.push(9000);

//ENUM
enum Color {
  rojo = "Rojo",
  verde = "Verde",
  azul = "Azul",
}
let colorFavorito: Color = Color.azul;
console.log(`Mi color favorito es ${colorFavorito}`);

//ANY

//Define una variable como any, lo que le permite adquirir un valor de string, number, array, etc.
let comodin: any = "Joker";
comodin = { type: "Wildcard" };

//OBJECT
let someObject: object = { type: "wildCard" };

//FUNCIONES

//Definir el tipo de datos de entrada
function add(a: number, b: number) {
  return a + b;
}
const sum = add(2, 3);

//Definir el tipo de datos de salida
function createAdder(a: number): (number) => number {
  return function (b: number) {
    return a + b;
  };
}
const addFour = createAdder(4);
const fourPlusSix = addFour(6);

//Hacer opcional un dato de entrada con ?
function fullName(
  firstName: string,
  lastName?: string,
  valorDefault: string = "valorAgregadoPorDefault"
): string {
  return `${firstName} ${lastName} ${valorDefault}`;
}
const richard = fullName("Richard");
console.log(richard);

//INTERFACES
interface Rectangulo {
  alto: number;
  ancho: number;
}
let rect: Rectangulo = {
  alto: 4,
  ancho: 6,
};
function area(r: Rectangulo): number {
  return r.alto * r.ancho;
}
const areaRect = area(rect);
console.log(areaRect);
